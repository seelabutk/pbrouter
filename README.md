# PBRouter #

### Summary ###

Send [Protocol Buffers](https://code.google.com/p/protobuf/) between a browser client and a server implemented in a language with native Protocol Buffer support (C++, Python, or Java).

### Dependencies ###

* [Protocol Buffers](https://github.com/dcodeIO/ProtoBuf.js)
* [Tornado](http://www.tornadoweb.org/en/stable/)
* [ProtoBuf.js](https://github.com/dcodeIO/ProtoBuf.js)

### Install instructions ###

### Testing ###

### Examples ###

### Documentation ###

### Want to help? ###

We would love to have you contribute to the project.  Please send an email if you are interested (see below for contact info).

### Contact Us! ###

John Duggan: jduggan1@vols.utk.edu