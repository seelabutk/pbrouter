import tornado.web
import tornado.websocket
import socket
import sys

def server_listen(router):
	min_str_size = 4
	while not router.stop_thread: # TODO: see router.py about placeholder
		msg = router.to_server_socket.recv(min_str_size)
		# TODO: finish this function

class Application(tornado.web.HTTPServer, options):
	def __init__(self):
		settings = {
			'address_family' : socket.AF_INET,
			'socket_type' : socket.SOCK_STREAM,
			'protocol_number' : 0,
			'server_port' : 5555,
			'server_host' : 'localhost',
			'client_port' : 5556,
			'client_host' : 'localhost',
		}

		# TODO: Can we remove this requirement and the reencoding of messages from server?
		try:
			settings['proto_module'] = options['proto_module']
		except KeyError:
			sys.stderr.write('PBRouter: ERROR: No protobuf module specified. Specify option proto_module to fix this.\n')
			return None

		for key in settings:
			try:
				settings[key] = options[key]
			except KeyError:
				pass

		handlers = [(r'/websocket', WebSocketHandler)]

		return tornado.web.Application.__init__(self, handlers, **settings)

	def set_setting(self, setting, value):
		self.settings[setting] = value

	def get_setting(self, setting):
		return self.settings[setting]

class WebSocketHandler(tornado.websocket.WebSocketHandler):
	def open(self):
		print 'PBRouter: Connection to client established.'

	# message received from client, forward to server
	def on_message(self, msg):
		# parse message (get size)
		size, message = msg.split(':', 1)
		size = int(size)

		# send request to server
		totalsent = 0
		while totalsent < size:
			sent = self.to_server_socket.send(message)
			totalsent += sent
			print 'PBRouter: Sent ' + str(sent) + ' bytes to server.'

	def on_close(self):
		print 'PBRouter: Connection to client closed.'
