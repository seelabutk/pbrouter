import socket
import pbrouter_utils
import tornado.ioloop
import sys
import threading

# TODO: How can we enable multiple clients?  Is this even our problem?

class PBRouter(options):
	def __init__(self):
		# create app and settings
		router = Application(options)
		__import__(router.settings['proto_module'])

		# set up connection to server and begin waiting for messages
		router.to_server_socket = socket.socket(router.settings['address_family'], router.settings['socket_type'], router.settings['protocol_number'])
		router.to_server_socket.connect(router.settings['server_port'], router.settings['server_host'])
		router.stop_thread = False # TODO: fix this placeholder, how do I stop the thread
		router.server_thread = threading.Thread(target=server_listen, args=[router])
		router.server_thread.start()

		# set up listener for clients
		router.listen(router.settings['client_port'], router.settings['client_host'])
		tornado.ioloop.IOLoop.instance().start()

	def stop(self):
		router.stop_thread = True # TODO: see above about placeholder
		router.to_server_socket.close()
		tornado.ioloop.IOLoop.instance().stop()
